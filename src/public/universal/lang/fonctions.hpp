/**
 * \file fonctions.hpp
 * \brief Alias de fonctions.
 * \version 0.1
 * \date 2022-10-10
 *
 * \copyright Copyright (c) 2022
 *
 */
#pragma once

namespace universal::lang {

/**
 * \brief Classe prête à réaliser une opération.
 *
 * L’objet contient toutes les informations nécessaires à son exécution.
 * L’exécution peut être déclenchée plusieurs fois mais ne garantie pas d’être identique et de
 * produire les mêmes actions.
 */
using Executable = void ( & )();

template<typename... T>
using Condition = bool ( & )( T &&... );
using IntCondition = Condition<int>;
using DoubleCondition = Condition<double>;

/**
 * \brief Fonction capable de comparer deux objets.
 *
 * \tparam T type de l’objet inférieur
 * \tparam T2 type de l’objet supérieur
 * \retval true si obj_inf est strictement inférieur à obj_sup, selon la méthode de comparaison.
 */
template<typename T, typename T2 = T>
using Comparateur = bool ( & )( T const & obj_inf, T2 const & obj_sup );

/**
 * \brief Fonction consommant des valeurs mais sans en produire à son tour.
 *
 * Une telle fonction ne doit pas être utilisée avec des références non \em const car elle n’est pas
 * destinée à modifier le contenu de ses arguments.
 * \tparam T
 */
template<typename... T>
using Consommateur = void ( & )( T &&... );

/**
 * \brief Fonction capable de produire indéfiniment des valeurs.
 *
 * La création d’une valeur peut modifier l’état interne de la fonction.
 * Les valeurs produites peuvent être différentes et il n’y a aucune garantie d’obtenir une valeur
 * en particulier au i-ème appel de la fonction.
 * \tparam T type des objets produits.
 */
template<typename T>
using Producteur = T ( & )();

}  // namespace universal::lang
