#pragma once

#include <cassert>
#include <cstddef>	// std::size_t
#include <memory>  // std::shared_ptr
#include <tuple>  // std::tuple
#include <type_traits>

namespace universal::lang {

template<typename P>
struct allocation_result {
	P ptr;
	std::size_t count;
};

/**
 * \brief Interface pour les Allocator.
 *
 * Cette interface respecte le concept Allocator.
 * Les implémentations utilisant cette interface sont interchangeables et peuvent être entreposés
 * dynamiquement.
 *
 * \tparam T type d’objet
 * \see https://en.cppreference.com/w/cpp/named_req/Allocator
 */
template<typename T>
struct Allocator {
public:
	using value_type = T;
	using pointer = T *;
	using size_type = std::size_t;
	using is_always_equal = std::false_type;

public:
	constexpr Allocator() noexcept = default;
	constexpr Allocator( Allocator && ) noexcept = default;
	constexpr Allocator( Allocator const & ) noexcept = default;
	virtual ~Allocator() noexcept = default;

	constexpr Allocator & operator=( Allocator && ) noexcept = default;
	constexpr Allocator & operator=( Allocator const & ) noexcept = default;

	/**
	 * \brief Alloue un espace mémoire pour un tableau d’objet T[].
	 *
	 * \param n nombre d’objets T dans le tableau.
	 * \param hint indicateur de voisinage.
	 * Cette indicateur encourage l’Allocator à fournir un emplacement mémoire proche mais n’est pas
	 * contraignant.
	 * \return un pointeur non nul vers le premier élément
	 * \throw std::bad_alloc si l’allocation échoue.
	 */
	virtual pointer allocate( size_type n, void const * hint = nullptr ) = 0;

	/**
	 * \brief Alloue un espace mémoire d’au moins N objets T.
	 *
	 * Cette fonction peut être utilisée par les systèmes qui réserve de la mémoire à l’avance comme
	 * std::vector.
	 *
	 * \param n nombre minimum d’éléments dans le tableau T[]
	 * \param hint indicateur de voisinage
	 * \return l’emplacement alloué et le nombre d’élément.
	 * \throw std::bad_alloc si l’allocation échoue
	 */
	virtual allocation_result<pointer> allocate_at_least( size_type n, void const * hint = nullptr )
		= 0;

	/**
	 * \brief Demande la réallocation d’un tableau T[]
	 *
	 * La réallocation permet de modifier la taille du tableau de n à m éléments.
	 * La mémoire associée peut être déplacée. Un déplacement ce fera avec std::memcpy et sans appel
	 * à des opérations de déplacement comme un <em>move constructor</em>.
	 *
	 * Cette fonction est particulièrement utile pour les implémentations de compression mémoire si
	 * l’Allocator est capable de réduire les espaces inutilisés.
	 *
	 * \param ptr ancien emplacement mémoire
	 * \param n ancien nombre d’éléments
	 * \param m nouvelle quantité
	 * \retval nullptr si la réallocation échoue. Dans ce cas l’ancien pointeur est toujours valide.
	 * \sa https://en.cppreference.com/w/cpp/string/byte/memcpy
	 */
	virtual pointer reallocate( pointer ptr, size_type n, size_type m ) noexcept = 0;

	/**
	 * \brief Libère la mémoire à l’emplacement indiqué.
	 *
	 * Cette mémoire doit avoir alloué précédemment par ce même Allocator ou un Allocator évalué
	 * égal ( \c operator== ). \param n nombre d’éléments dans le tableau alloué.
	 */
	virtual void deallocate( pointer, size_type n ) = 0;

	bool operator==( Allocator<T> const & alloc ) const noexcept {
		return this == &alloc;
	}
};	//// Allocator

template<typename T>
class PolymorphicAllocator : public Allocator<T> {
public:
	using typename Allocator<T>::value_type;
	using typename Allocator<T>::pointer;
	using typename Allocator<T>::size_type;

	using is_always_equal = std::false_type;
	using allocator_t = Allocator<std::byte>;

private:
	std::shared_ptr<allocator_t> const _allocator;

public:
	PolymorphicAllocator( std::shared_ptr<allocator_t> alloc ) noexcept;

	template<typename U>
	PolymorphicAllocator( PolymorphicAllocator<U> const & alloc ) noexcept;
	PolymorphicAllocator( PolymorphicAllocator const & ) noexcept = default;

	template<typename U>
	struct rebind {
		using other = PolymorphicAllocator<U>;
	};

public:
	pointer allocate( size_type n, void const * hint = nullptr ) final;

	allocation_result<pointer> allocate_at_least( size_type n, void const * hint = nullptr ) final;

	pointer reallocate( pointer ptr, size_type o_size, size_type n_size ) noexcept final;

	void deallocate( pointer ptr, size_type n ) final;

public:
	bool operator==( PolymorphicAllocator const & poly_alloc ) const;

	auto const & get_allocator() const {
		return _allocator;
	}
};	//// PolymorphicAllocator

template<typename T>
class SharedAllocator : public Allocator<T> {
public:
	using typename Allocator<T>::value_type;
	using typename Allocator<T>::pointer;
	using typename Allocator<T>::size_type;

	using is_always_equal = std::false_type;
	using allocator_t = Allocator<T>;

public:
	SharedAllocator( std::shared_ptr<Allocator<T>> );
	SharedAllocator( SharedAllocator const & ) = default;
	~SharedAllocator() noexcept = default;

	SharedAllocator( SharedAllocator && ) = delete;
	SharedAllocator & operator=( SharedAllocator const & ) = delete;

	pointer allocate( size_type n, void const * hint = nullptr ) final;
	allocation_result<pointer> allocate_at_least( size_type n, void const * hint = nullptr ) final;
	pointer reallocate( pointer ptr, size_type o_size, size_type n_size ) noexcept final;
	void deallocate( pointer ptr, size_type n ) final;

	bool operator==( SharedAllocator const & ) const;

	auto const & get_allocator() const {
		return _allocator;
	}

	/** Redirige vers l’allocation par défaut si le type est différent. */
	template<typename U>
	struct rebind {
		using other
			= std::conditional_t<std::is_same_v<T, U>, SharedAllocator<T>, std::allocator<U>>;
	};

	template<typename U>
	operator std::allocator<U>() const {
		return {};
	}

private:
	std::shared_ptr<Allocator<T>> const _allocator;
};	//// SharedAllocator

//
// ---------- Details ----------
//
template<typename T>
PolymorphicAllocator<T>::PolymorphicAllocator( std::shared_ptr<allocator_t> alloc ) noexcept :
	_allocator( std::move( alloc ) ) {
	assert( _allocator );
}

template<typename T>
template<typename U>
PolymorphicAllocator<T>::PolymorphicAllocator( PolymorphicAllocator<U> const & alloc ) noexcept :
	PolymorphicAllocator( alloc.get_allocator() ) {
}

template<typename T>
inline typename PolymorphicAllocator<T>::pointer
PolymorphicAllocator<T>::allocate( size_type n, void const * hint ) {
	return (pointer)_allocator->allocate( sizeof( T ) * n, hint );
}

template<typename T>
inline allocation_result<typename PolymorphicAllocator<T>::pointer>
PolymorphicAllocator<T>::allocate_at_least( size_type n, void const * hint ) {
	auto alloc = _allocator->allocate_at_least( sizeof( T ) * n, hint );
	return { (pointer)alloc.ptr, alloc.count / sizeof( T ) };
}

template<typename T>
inline typename PolymorphicAllocator<T>::pointer
PolymorphicAllocator<T>::reallocate( pointer ptr, size_type o_size, size_type n_size ) noexcept {
	return (pointer)_allocator->reallocate(
		(std::byte *)ptr, sizeof( T ) * o_size, sizeof( T ) * n_size );
}

template<typename T>
inline void PolymorphicAllocator<T>::deallocate( pointer ptr, size_type n ) {
	return _allocator->deallocate( (std::byte *)ptr, sizeof( T ) * n );
}

template<typename T>
bool PolymorphicAllocator<T>::operator==( PolymorphicAllocator<T> const & poly_alloc ) const {
	return this->_allocator.get() == poly_alloc._allocator.get()
		|| *this->_allocator == *poly_alloc._allocator;
}

//
// SharedAllocator
//
template<typename T>
SharedAllocator<T>::SharedAllocator( std::shared_ptr<Allocator<T>> alloc ) :
	_allocator( std::move( alloc ) ) {
}

template<typename T>
inline typename SharedAllocator<T>::pointer
SharedAllocator<T>::allocate( size_type n, void const * hint ) {
	return this->_allocator->allocate( n, hint );
}

template<typename T>
inline allocation_result<typename SharedAllocator<T>::pointer>
SharedAllocator<T>::allocate_at_least( size_type n, void const * hint ) {
	return this->_allocator->allocate_at_least( n, hint );
}

template<typename T>
inline typename SharedAllocator<T>::pointer
SharedAllocator<T>::reallocate( pointer ptr, size_type o_size, size_type n_size ) noexcept {
	return this->_allocator->reallocate( ptr, o_size, n_size );
}

template<typename T>
inline void SharedAllocator<T>::deallocate( pointer ptr, size_type n ) {
	return this->_allocator->deallocate( ptr, n );
}

template<typename T>
inline bool SharedAllocator<T>::operator==( SharedAllocator<T> const & s_alloc ) const {
	return this->_allocator.get() == s_alloc._allocator.get()
		|| *this->_allocator == *s_alloc._allocator;
}

}  // namespace universal::lang
