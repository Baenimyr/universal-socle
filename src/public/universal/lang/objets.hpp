/**
 * \file objets.hpp
 * \brief Définitions d’interfaces pour augmenter les classes C++.
 * \version 0.1
 * \date 2022-10-10
 *
 * \copyright Copyright (c) 2022
 *
 */
#pragma once

#include <memory>  // std::unique_ptr

namespace universal::lang {

/**
 * \brief Classe générique destinée à l’héritage.
 */
struct Objet {
	constexpr Objet() noexcept = default;
	virtual ~Objet() noexcept = default;
	constexpr Objet( Objet && ) noexcept = default;
	constexpr Objet( Objet const & ) noexcept = default;

	constexpr Objet & operator=( Objet && ) noexcept = default;
	constexpr Objet & operator=( Objet const & ) noexcept = default;
};	//// Objet

/** \brief Permet la duplication de l’objet sans accès au constructeur par copie. */
template<typename Self>
struct Cloneable : public virtual Objet {
public:
	inline std::unique_ptr<Cloneable<Self>> clone() const {
		return std::unique_ptr<Cloneable<Self>>( this->_clone() );
	}

protected:
	virtual Self * _clone() const = 0;
};	//// Cloneable

/** \brief Interface d’un objet pouvant être clos.
 *
 * La fonction \link close() \endlink permet la libération de ressources ou la fermeture d’autres
 * composants. L’objet n’est pas détruit et certains composants sont toujours disponibles. C’est le
 * cas des indicateurs d’état. Le comportement de certaines fonctions devient indéfini après
 * fermeture.
 * Dans des cas particuliers, l’objet peut être ouvert à nouveau.
 */
struct Closeable : public virtual Objet {
	virtual void close() = 0;
};	//// Closeable

/**
 * @brief Objet comparable de manière dynamique.
 *
 * Cet objet peut être comparé à gauche (sans égalité stricte) au type T.
 */
template<typename T>
struct Comparable : public virtual Objet {
	virtual bool operator<( T const & ) const noexcept = 0;
};	//// Comparable

}  // namespace universal::lang

namespace std {

template<typename T>
bool operator<(
	std::unique_ptr<universal::lang::Comparable<T>> const & val_1,
	T const & val_2 ) {
	if( !val_1 ) {
		return true;
	}
	return *val_1 < val_2;
}

template<typename T>
bool operator<(
	std::shared_ptr<universal::lang::Comparable<T>> const & val_1,
	T const & val_2 ) {
	if( !val_1 ) {
		return true;
	}
	return *val_1 < val_2;
}

}  // namespace std
