/**
 * \file collections.hpp
 * \brief Ensemble d’interfaces décrivant des collections.
 * \version 0.1
 * \date 2022-11
 */
#pragma once
#include "universal/lang/objets.hpp"

#include <cstddef>
#include <type_traits>

namespace universal::lang {

/**
 * \brief Conteneur dans lequel il est possible d’ajouter des objets.
 *
 * L’objet est construit en place selon le type de paramètres proposés.
 **/
template<typename... Args>
struct Injectable : public virtual Objet {
	/**
	 * \brief Ajoute un élément à la collection.
	 *
	 * L’ajout se fait à l’emplacement par défaut pour la collection.
	 * Certaines collection ou implémentation peuvent définir d’autres méthodes (\c push_back, \c
	 * insert, ...) pour ajouter des éléments à d’autres emplacements.
	 *
	 * \return true si l’élément à bien été ajouté
	 * \return false si l’élément a été rejeté par la collection
	 **/
	virtual bool add( Args &&... constructeurs ) const noexcept = 0;
};	//// Injectable

/**
 * \brief Conteneur dans lequel les objets peuvent être retirés.
 *
 * L’élément à retirer est désigné par les paramètres de la fonction.
 * La fonction de suppression peut retourner une valeur, comme un indicateur de réussite, l’élément
 *retiré, un \em iterator voisin, etc.
 *
 * \tparam R type de réponse
 * \tparam Args conditions pour désigner le ou les objets à retirer.
 **/
template<typename R, typename... Args>
struct Prelevable : public virtual Objet {
	virtual R erase( Args ... conditions ) = 0;
};	//// Prelevable

/** \brief Classe générique des collections.
 *
 * Toute collection contient un nombre variables d’éléments. Les collections ont une taille et une
 * taille maximale, généralement à cause des limitations du système.
 *
 * Cette interface ne définit pas de fonctions d’accès car il n’est pas précisé si les éléments sont
 * ordonnés ni même accessibles à la demande.
 * Les arguments nécessaires à la création ne sont pas toujours disponibles et certaines fonctions
 * ne peuvent pas être implémentées par toutes les collections.
 *
 * Les fonctions fournies ici sont par défaut sensibles aux modifications concurrentes.
 */
struct Collection : public virtual Objet {
	using size_type = std::size_t;

public:	 // -- Capacité
	/**
	 * \brief Teste si la collection est vide.
	 *
	 * Cette fonction est plus simple et rapide que de comparer \em size() à 0.
	 */
	virtual bool empty() const noexcept = 0;

	/**
	 * \brief Nombre d’éléments dans cette collection.
	 *
	 * La fonction peut connaître la valeur ou devoir la calculer. Utiliser cette fonction n’est pas
	 * anodin.
	 */
	virtual size_type size() const = 0;

	/**
	 * \brief Nombre maximum d’élément pouvant être contenus dans cette collection.
	 *
	 * La valeur dépend des limitations du système ou de l’implémentation. Tenter de placer plus
	 * d’éléments dans la collection va probablement déclencher des erreurs. En général, ce nombre
	 * n’est jamais atteint et vaut \em std::numeric_limits<size_t>::max().
	 * \see https://en.cppreference.com/w/cpp/types/numeric_limits
	 */
	virtual size_type max_size() const = 0;

public:	 // -- Modification
	/**
	 * \brief Vide la collection et efface tout son contenu.
	 */
	virtual void clear() = 0;
};	//// Collection

/**
 * \brief Collection dont les éléments sont accessibles par leur position.
 *
 * Les éléments de la collection sont associés à un index entre 0 et \c size() - 1 de façon
 * continue.
 *
 * L’insertion ou la suppression d’un élément de la collection peut modifier tous les autres
 * indices. La modification du nombre d’élément engendre donc une invalidation des pointeurs et
 * itérateurs.
 */
template<typename T>
struct Liste :
	public virtual Collection,
	public virtual Prelevable<void, Collection::size_type> {
	using size_type = Collection::size_type;
	using value_type = std::decay_t<T>;
	using reference = value_type &;
	using const_reference = value_type const &;
	using pointer = value_type *;
	using const_pointer = value_type const *;

public:	 // -- Accès
	/** \brief Obtient une référence vers l’objet i de la liste.
	 *
	 * Utiliser un index supérieur ou égal à \ref size() est indéfini.
	 * \param pos index de l’objet.
	 */
	virtual reference operator[]( size_type pos ) = 0;

	/** \brief Obtient une référence const vers l’objet i de la liste.
	 *
	 * Utiliser un index supérieur ou égal à \ref size() est indéfini.
	 * \param pos index de l’objet.
	 */
	virtual const_reference operator[]( size_type pos ) const = 0;

public:	 // -- Accès spéciaux
	/** \brief Référence vers le premier élément de la liste.
	 *
	 * Le comportement de cette fonction n’est pas défini pour une liste vide.
	 */
	virtual reference front() {
		return this->operator[]( 0 );
	}

	virtual const_reference front() const {
		return this->operator[]( 0 );
	}

	/** \brief Référence vers le dernier élément de la liste.
	 *
	 * Le comportement de cette fonction n’est pas défini pour une liste vide.
	 */
	virtual reference back() {
		return this->operator[]( this->size() - 1 );
	}

	virtual const_reference back() const {
		return this->operator[]( this->size() - 1 );
	}

	/**
	 * \brief Efface l’élément désigné par sa position.
	 **/
	void erase( size_type ) override = 0;
};	//// Liste

/**
 * \brief Collection en séquence permettant de lire et de retirer l’élément de tête (FIFO ou LIFO).
 *
 * Les éléments d’une file sont destinés à être déplacés d’un producteur vers un consommateur.
 * La meilleur façon est d’utiliser un \em unique_ptr ou un \em shared_ptr.
 * \tparam T doit satisfaire is_move_constructible ou is_copy_constructible.
 * \see https://en.cppreference.com/w/cpp/container/queue
 */
template<typename T>
struct Queue : public virtual Collection, public virtual Prelevable<T> {
	static_assert( std::is_move_constructible<T>::value || std::is_copy_constructible<T>::value );
	using size_type = Collection::size_type;
	using value_type = std::decay_t<T>;
	using reference = value_type &;
	using const_reference = value_type const &;

public:
	/**
	 * \brief Accède au premier élément de la file sans le retirer.
	 *
	 * Le comportement si la file est vide est indéfini. L’utilisateur d’abord doit vérifier que la
	 * queue n’est pas vide
	 */
	virtual reference front() = 0;
	virtual const_reference front() const = 0;

	/**
	 * \brief Supprime et retourne le premier élément.
	 *
	 * Il est déconseillé de lire le premier élément puis de le supprimer par une deuxième étape car
	 * il pourrait avoir été modifié.
	 * \see https://en.cppreference.com/w/cpp/utility/move_if_noexcept
	 */
	value_type erase() noexcept(
		std::is_nothrow_move_constructible<value_type>::value
		|| std::is_nothrow_copy_constructible<value_type>::value ) override
		= 0;
};	//// Queue

}  // namespace universal::lang
