#pragma once

#include "universal/lang/allocator.hpp"

#include <list>
#include <map>
#include <memory>  // std::unique_ptr
#include <set>
#include <type_traits>
#include <unordered_map>
#include <unordered_set>
#include <vector>


namespace universal::memory {

/**
 * \brief Fonction \c delete associée à un Allocator.
 *
 * Permet d’utiliser une Allocator sur les types gérant la libération mémoire, comme
 * std::unique_ptr.
 *
 * \tparam Alloc
 * \tparam N nombre d’éléments s’il s’agit d’un tableau T[N].
 */
template<typename Alloc, std::size_t N = 1>
class Allocator_delete {
private:
	Alloc _allocator;

public:
	Allocator_delete( Alloc const & alloc ) :
		_allocator( alloc ) {
	}

	template<typename Alloc_>
	Allocator_delete( Allocator_delete<Alloc_, N> const & a_del ) :
		_allocator( a_del.allocator() ) {
	}

	auto & allocator() {
		return _allocator;
	}

	auto const & allocator() const {
		return _allocator;
	}

	void operator()( typename Alloc::pointer ptr ) {
		_allocator.deallocate( ptr, N );
	}
};

template<typename T, typename Allocator, typename... Args>
auto allocate_unique( Allocator & alloc, Args &&... args ) {
	static_assert( !std::is_array_v<T> );
	static_assert( std::is_same_v<T, typename std::decay_t<Allocator>::value_type> );
	T * data = alloc.allocate( 1 );
	std::allocator_traits<Allocator>::construct( alloc, data, std::forward<Args>( args )... );
	return std::unique_ptr<T, Allocator_delete<Allocator, 1>>( data, alloc );
}

namespace pma {
template<typename T>
using vector = ::std::vector<T, universal::lang::PolymorphicAllocator<T>>;

template<typename T>
using list = ::std::list<T, universal::lang::PolymorphicAllocator<T>>;

template<typename K, typename V, typename Cmp = std::less<K>>
using map = ::std::map<K, V, Cmp, lang::PolymorphicAllocator<std::pair<K const, V>>>;

template<class Key, class T, class Compare = std::less<Key>>
using multimap
	= ::std::multimap<Key, T, Compare, lang::PolymorphicAllocator<std::pair<Key const, T>>>;

template<class Key, class T, class Hash = std::hash<Key>, class KeyEqual = std::equal_to<Key>>
using unordered_map = ::std::unordered_map<
	Key, T, Hash, KeyEqual, lang::PolymorphicAllocator<std::pair<Key const, T>>>;

template<typename V, typename Cmp = std::less<V>>
using set = ::std::set<V, Cmp, lang::PolymorphicAllocator<V>>;

template<class Key, class Compare = std::less<Key>>
using multiset = ::std::multiset<Key, Compare, lang::PolymorphicAllocator<Key>>;

template<class Key, class Hash = std::hash<Key>, class KeyEqual = std::equal_to<Key>>
using unordered_set = ::std::unordered_set<Key, Hash, KeyEqual, lang::PolymorphicAllocator<Key>>;
}  // namespace pma

}  // namespace universal::memory
