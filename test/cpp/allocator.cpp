#include "universal/lang/allocator.hpp"

#include "universal/lang/memory.hpp"

#include <atomic>
#include <cstddef>
#include <cstdlib>
#include <gtest/gtest.h>
#include <list>
#include <memory>  // std::allocator_traits
#include <string>
#include <type_traits>
#include <unordered_map>

using namespace universal::lang;

template<typename T>
class SystemAllocator : public Allocator<T> {
	std::allocator<T> alloc;
	using alloc_traits = std::allocator_traits<decltype( alloc )>;

public:
	using value_type = T;
	using pointer = T *;
	std::atomic_uint64_t allocations = 0;

public:
	constexpr SystemAllocator() noexcept = default;

	std::allocator<T> const & allocator() const {
		return alloc;
	}

	T * allocate( std::size_t n, void const * hint = nullptr ) final;

	allocation_result<pointer>
	allocate_at_least( std::size_t n, void const * hint = nullptr ) final;

	T * reallocate( T * ptr, std::size_t o_size, std::size_t n_size ) noexcept final;

	void deallocate( T * ptr, std::size_t n ) final;
};

template<typename T>
T * SystemAllocator<T>::allocate( std::size_t n, void const * hint ) {
	allocations += n;
	return this->alloc.allocate( n );
}

template<typename T>
allocation_result<T *> SystemAllocator<T>::allocate_at_least( std::size_t n, void const * hint ) {
	allocations += n;
	return { this->alloc.allocate( n ), n };
}

template<typename T>
T * SystemAllocator<T>::reallocate( T * ptr, std::size_t o_size, std::size_t n_size ) noexcept {
	assert( n_size > 0 );
	T * n_ptr = reinterpret_cast<T *>( std::realloc( ptr, sizeof( T ) * n_size ) );
	if( n_ptr != nullptr ) allocations += ( n_size - o_size );
	return n_ptr;
}

template<typename T>
void SystemAllocator<T>::deallocate( T * ptr, std::size_t n ) {
	allocations -= n;
	return this->alloc.deallocate( ptr, n );
}

//
//
//

TEST( Allocator, system ) {
	auto allocator = std::make_shared<SystemAllocator<int>>();

	std::vector<int, SharedAllocator<int>> data( ( SharedAllocator<int>( allocator ) ) );
	std::uint64_t allocation_init = allocator->allocations;
	EXPECT_EQ( allocator, data.get_allocator().get_allocator() );
	EXPECT_EQ( 0, allocator->allocations - allocation_init );
	data.reserve( 17 );
	EXPECT_EQ( 17, allocator->allocations - allocation_init );
	data.push_back( 489 );
	EXPECT_EQ( 17, allocator->allocations - allocation_init );
	data.clear();
	data.shrink_to_fit();
	EXPECT_EQ( 0, allocator->allocations - allocation_init );
}

TEST( Allocator, unique ) {
	auto sys_alloc = std::make_shared<SystemAllocator<double>>();
	SharedAllocator<double> allocator( sys_alloc );

	auto data = universal::memory::allocate_unique<double>( allocator, 89.0 );
	EXPECT_EQ( 1, sys_alloc->allocations );
	data.reset();
	EXPECT_EQ( 0, sys_alloc->allocations );
}

TEST( Allocator, polymorphe ) {
	auto sys_alloc = std::make_shared<SystemAllocator<std::byte>>();

	universal::memory::pma::unordered_map<std::string, int> map(
		0, universal::lang::PolymorphicAllocator<std::pair<std::string const, int>>( sys_alloc ) );
	universal::memory::pma::list<std::string> list(
		( universal::lang::PolymorphicAllocator<std::string>( sys_alloc ) ) );
	map.max_load_factor( 3 );
	map.emplace( "2011", 2011 );
	map.emplace( "2017", 17 );
	map.emplace( "2022", 2022 );
	EXPECT_NE( 0, sys_alloc->allocations );
	map.clear();
	map.reserve( 0 );
	map.rehash( 0 );
	// EXPECT_EQ( 0, sys_alloc->allocations );
}
